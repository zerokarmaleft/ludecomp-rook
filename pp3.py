#!/usr/bin/env python

"""
Driver for Programming Problem 3 of CS 3513, Spring 2012.

The programming problem is to use LU factorization with rook pivoting
to solve systems of equations.

Reads two matrices A and B then solves AX=B.

Written by Doug Heisterkamp
Modified by Edward Cho
Last updated: 2012-04-04
"""

from numpy import *
from numpy.linalg import *
import LUdecomp
import sys

eps = 1e-12

# default file names
dataAname = "pp3DataA.txt"
dataBname = "pp3DataB.txt"

# display file names
if len(sys.argv) > 1:
   dataAname = sys.argv[1]
   print "Using data file " + dataAname + " for matrix A"
if len(sys.argv) > 2:
   dataBname = sys.argv[2]
   print "Using data file " + dataBname + " for matrix B"

# check for valid text files
try:
   A = asmatrix(loadtxt(dataAname,dtype=float))
except Exception as e:
   print "Failed to load matrix A data file," + dataAname + ".  Exception : " + e
   sys.exit(-1)
try:
   B = asmatrix(loadtxt(dataBname,dtype=float))
except Exception as e:
   print "Failed to load matrix B data file," + dataBname + ".  Exception : " + e
   sys.exit(-1)

# save original system to check computed solution
a_orig = A.copy()
b_orig = B.copy()

# find LU factorization
LU,p,q,flag,mes = LUdecomp.decompose(A)
if not flag :
   print "\nFailed to factor matrix A.\nError message = " + mes
   print "Initial A = \n" + a_orig
   print "At failure, A = \n" + A
   print "with partial p = \n" + p
   sys.exit(-2)

# display LU factorization
print "\nLU factorization of A, stored in A as \n"
print LU;
print "\nwith perm vector p = " + str(p)
print "and  perm vector q = " + str(q)

# find X for AX = B
X, sflag, serror = LUdecomp.solve(LU,B,p,q)
if not sflag :
   print "\nFailed to use LU factors to solve AX = B. Error message = " + mes
   sys.exit(-3)

# display solution
print "\nSolution of AX = B is X = \n"
print X;

# display residuals
print "\nResiduals of system of equations:\n"
for i in range(X.shape[1]):
   print "    || A X[:,i] - B[:,i] ||_2 = " + str(norm(a_orig*X[:,i]-b_orig[:,i]))
print "\nor as matrix norm, ||A X - B||_F = " + str(norm(a_orig*X-b_orig))
print "or as matrix norm, ||A X - B||_2 = " + str(norm(a_orig*X-b_orig,2))
