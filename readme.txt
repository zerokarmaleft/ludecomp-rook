Edward Cho
Dr. Doug Heisterkamp
CS 3513, Section 801
4 April 2012

Programming Problem #3
LU Factorization with Rook Pivoting
===================================

I implemented rook pivoting as a naive recursive function, which may
blow the stack for large datasets. If I had more time, I would have
converted it to an iterative function.

For some solutions, my vectors are out of order. I suspect that
applying or calculating column permutations is messed up somehow, but
I'm unable to track it down.

TODO: exampleA, exampleB (approximately close?)
pp3exer11A, pp3exer11B (correct)
TODO: pp3exer12A, pp3exer12B (in wrong order)
TODO: pp3exer13A, pp3exer13B (in wrong order)
pp3exer14A, pp3exer14B (correct)
pp3exer15A, pp3exer15B (correct)
