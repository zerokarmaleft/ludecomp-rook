from numpy import *

def rook_pivot(A, p=(0,0), axis=1):
    i, j = p
    # search column
    if axis == 1:
        i_max = abs(A[:,j]).argmax()
        if i == 0 and j == 0:
            return rook_pivot(A, (i_max, j), axis=0)
        elif i == i_max:
            return p
        else:
            return rook_pivot(A, (i_max,j), axis=0)
    # search row
    elif axis == 0:
        j_max = abs(A[i,:]).argmax()
        if j == j_max:
            return p
        else:
            return rook_pivot(A, (i,j_max), axis=1)
    # only works on nxn matrices
    else:
        raise ValueError

def swap(v, k, i):
    # swap entries in a vector
    temp = v[k]
    v[k] = v[i]
    v[i] = temp

def swap_row(A, i1, i2):
    row = A[i1, :].copy()
    A[i1, :] = A[i2, :].copy()
    A[i2, :] = row.copy()

def swap_column(A, j1, j2):
    col = A[:, j1].copy()
    A[:, j1] = A[:, j2].copy()
    A[:, j2] = col.copy()

def decompose(A):    
    # check A is a square matrix
    assert(A.shape[0] == A.shape[1])
    
    # number of rows/columns
    n = A.shape[0]
    
    # initialize permutation vectors
    p = []
    q = []
    
    LU = A.copy()    
    for k in range(n-1):
        # find row and column index of rook pivot
        i, j = rook_pivot(LU[k:, k:])
        
        # swap current row and pivot row
        swap_row(LU, k, i + k)
        p.append(i + k)
        
        # swap current column and pivot column
        swap_column(LU, k, j + k)
        q.append(j + k)
            
        # divide column by pivot
        LU[k+1:, k] /= LU[k, k]
            
        # fill inner matrix with outer product
        LU[k+1:, k+1:] -= LU[k+1:, k] * LU[k, k+1:]
    return LU, p, q, True, None

def apply_row_perm(B, p):
    Y = B.copy()
    for i in range(len(p)):
        swap_row(Y, i, p[i])
    return Y

def apply_col_perm(Z, q):
    X = Z.copy()
    for j in range(len(q)):
        swap_column(X, j, q[j])
    return X

def forward_subst(L, B):
    Y = B.copy()
    n = L.shape[0]

    # for every solution column vector
    for j in range(B.shape[1]):
        y = Y[:,j]
        # for every row in L except the first
        for k in range(1, n):
            y[k] -= dot(L[k, :k], y[0:k])

    return Y

def back_subst(U, Y):
    Z = Y.copy()
    n = U.shape[0]

    # for every solution column vector
    for j in range(Z.shape[1]):
        z = Z[:,j]
        # for every row in U except the last
        for k in range(n - 1, -1, -1):
            z[k] = (z[k] - dot(U[k, k+1:n], z[k+1:n])) / U[k, k]

    return Z

def solve(A, B, p, q):
    PB = apply_row_perm(B, p)
    Y  = forward_subst(A, PB)
    QZ = back_subst(A, Y)
    X  = apply_col_perm(QZ.T, q)
    
    return X.T, True, None

if __name__ == "__main__":
    A = asmatrix(loadtxt("test/pp3exer11A.txt"))
    B = asmatrix(loadtxt("test/pp3exer11B.txt"))
    LU, p, q, flag, err = decompose(A)
    print "[L][U] =\n" + str(LU) + "\n"
    print "{p} = " + str(p) + "\n"
    print "{q} = " + str(q) + "\n"
    print "[B] =\n" + str(B) + "\n"
    print solve(LU, B, p, q)
